/*
 * @Author: hucheng
 * @Date: 2020-05-10 15:25:18
 * @Description: 演示上传到 阿里云oss
 */
const fs = require("fs");
const path = require("path");
const OSS = require("ali-oss");
const CWD = process.cwd();
const logger = log();
const config = {
  ossConfig: {
    accessKeyId: "",
    accessKeySecret: "",
    region: "",
    bucket: "",
  }
};
let distDir
try {
  distDir  = process.argv[2] || "dist"
} catch (error) {
  distDir  =  "dist"
}

const projectName = process.env.$TEST_PROJECT_NAME || 'demo'; //  TEST_PROJECT_NAME   是在 .gitlab-ci.yml 中 variables 属性 配置的
logger.info("模拟上传到oss");
logger.info("uploading.......")
logger.info("upload success")

//deploy(projectName);
sendMessage();
function deploy(projectName) {
  const projectConfig = config.projectConfig;
  const filePathArray = getFilePathForDir(
    path.resolve(CWD, distDir),
    []
  );
  filePathArray.map((ele) => {
    uploadALiOss(`/${projectName}/${ele.split("/").pop()}`, ele);
  });
}
async function uploadALiOss(key, file) {
  const ossConfig = config.ossConfig;
  const client = new OSS({
    region: ossConfig.region,
    accessKeyId: ossConfig.accessKeyId,
    accessKeySecret: ossConfig.accessKeySecret,
    bucket: ossConfig.bucket,
  });
  const result = await client.put(`fe/${key}`, file);

  logger.info(`upload success!,url is ${result.url}`);
  return result.url;
}
/**
 *
 * @param {String} docPath
 * @param {Array} resultArray
 */
function getFilePathForDir(docPath, resultArray) {
  if (!fs.existsSync(docPath)) {
    throw new Error(`${docPath} is not exit!`);
  }
  const array = fs.readdirSync(docPath);
  array.forEach(function (ele, index) {
    let info = fs.statSync(docPath + "/" + ele);
    if (info.isDirectory()) {
      getFilePathForDir(docPath + "/" + ele, resultArray);
    } else {
      resultArray.push(path.join(docPath, ele));
    }
  });
  return resultArray;
}
/**
 * 这里发送消息，一般公司都有自己的消息api，例如 企微，钉订，或者邮件
 */
function sendMessage(){
  logger.info("消息发送成功");
}
function log() {
  const baseMesasge = "gitlab ci: ";
  return {
    error: function (mesasge) {
      console.error(`${baseMesasge}${mesasge}`);
    },
    warn: function (mesasge) {
      console.warn(`${baseMesasge}${mesasge}`);
    },
    info: function (mesasge) {
      console.info(`${baseMesasge}${mesasge}`);
    },
    log: function (mesasge) {
      console.info(`${baseMesasge}${mesasge}`);
    },
  };
}
